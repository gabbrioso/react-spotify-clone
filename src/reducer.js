export const initialState = {
    user: null,
    playlist: [],
    playing: false,
    item: null,
    //REMOVE after finished developing...or change to NULL
    //token: "BQBiablwWEuCh5w-IN6qbTr7VzAl3QaymK059inICYgT5wHVt22dqnqKSus8yPo9GraOs61W9ufs-gmYecD3MPrpAEPP2nKIA_DXt3E4X0ph2sF1P1YrW-1McIDkuE62_HeCi7tK5hVWUqlYMqld4aGR9pVyeyyDrQ"
};

const reducer = (state, action) => {
    console.log(action);

    //Action -> type, [payload]

    switch (action.type) {
        case 'SET_USER':
            return {
                ...state,
                user: action.user,
            };
        case 'SET_TOKEN':
            return {
                ...state,
                token: action.token,
            };

        case "SET_PLAYLISTS":
            return {
                ...state,
                playlists: action.playlists,
            }

            case "SET_DISCOVER_WEEKLY":
                return {
                    ...state,
                    discover_weekly: action.discover_weekly,
                }
        default:
            return state;
    }
}

export default reducer;